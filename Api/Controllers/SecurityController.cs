﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utils;

namespace Api.Controllers
{
    [RoutePrefix(RoutePrefixes.Security)]
    public class SecurityController : Controller
    {
        [Route(Routes.Authenticate)]
        [HttpPost]
        public User Authorize( User user )
        {
            GenericRepository<User> userTable = new GenericRepository<Models.User>(new ApplicationDbContext());
            var all = userTable.GetAll();
            var outUser = userTable.FindBy( x => x.Email == user.Email && x.Password == user.Password ).FirstOrDefault();
            
            return outUser;
        }
    }
}