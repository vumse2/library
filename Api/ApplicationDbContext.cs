﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Api
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Author> Authors { get; set; }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Book> Books { get; set; }

        public ApplicationDbContext() : base( "ApplicationDbContext" )
        {
        }

        protected override void OnModelCreating( DbModelBuilder modelBuilder )
        {
            modelBuilder.Entity<Book>()
           .HasMany( b => b.Authors).WithMany( a => a.Books )
           .Map(
                    m =>
                    {
                        m.MapLeftKey( "BookId" );
                        m.MapRightKey( "AuthorId" );
                        m.ToTable( "BooksAuthors" );
                    }
            );
        }
    }
}
