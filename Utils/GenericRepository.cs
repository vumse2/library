﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public class GenericRepository<T> where T : class
    {
        protected DbContext context;
        protected readonly DbSet<T> dbSet;

        public GenericRepository( DbContext _context )
        {
            context = _context;
            dbSet = context.Set<T>();
        }

        public IList<T> GetAll( string includeProperties = "" )
        {
            IQueryable<T> query = dbSet.AsQueryable();

            foreach( string includeProperty in includeProperties.Split
                ( new char[] { StringConstants.Comma }, StringSplitOptions.RemoveEmptyEntries ) )
            {
                query = query.Include( includeProperty );
            }

            return query.ToList();
        }

        public IList<T> FindBy( System.Linq.Expressions.Expression<Func<T, bool>> predicate, string includeProperties = "" )
        {
            IQueryable<T> query = dbSet.Where( predicate ).AsQueryable();

            foreach( string includeProperty in includeProperties.Split
                ( new char[] { StringConstants.Comma }, StringSplitOptions.RemoveEmptyEntries ) )
            {
                query = query.Include( includeProperty );
            }

            return query.ToList();
        }

        public T GetSingle( System.Linq.Expressions.Expression<Func<T, bool>> predicate, string includeProperties = "" )
        {
            IQueryable<T> query = dbSet.Where( predicate ).AsQueryable();

            foreach( string includeProperty in includeProperties.Split
                ( new char[] { StringConstants.Comma }, StringSplitOptions.RemoveEmptyEntries ) )
            {
                query = query.Include( includeProperty );
            }

            T entity = query.FirstOrDefault();

            return entity;
        }

        public T Insert( T entity )
        {
            dbSet.Add( entity );
            context.SaveChanges();

            return entity;
        }

        public void Delete( T entity )
        {
            context.Entry( entity ).State = EntityState.Deleted;
        }

        public void Delete( System.Linq.Expressions.Expression<Func<T, bool>> predicate )
        {
            Delete( GetSingle( predicate ) );
        }

        public void Edit( T entity )
        {
            if( context.Entry( entity ).State == EntityState.Detached )
            {
                dbSet.Attach( entity );
            }
            context.Entry( entity ).State = EntityState.Modified;
        }
    }
}
