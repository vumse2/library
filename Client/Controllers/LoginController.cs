﻿using Client.Database;
using Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Resources;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using Utils;

namespace Client.Controllers
{
    [AllowAnonymous]
    [RoutePrefix( RoutePrefixes.Login )]
    public class LoginController : ControllerBase
    {
        public ActionResult LoginAsUser()
        {
            return View( ( Views.Login ) );
        }

        [Route( Routes.LoginPage )]
        [HttpGet]
        public ActionResult LoginPage()
        {
            return View( ( Views.Login ) );
        }

        [Route( Routes.Login )]
        [HttpPost]
        public async Task<ActionResult> Login( User user )
        {
            GenericRepository<User> userTable = new GenericRepository<Models.User>( new ApplicationDbContext() );
            var all = userTable.GetAll();
            var outUser = userTable.FindBy( x => x.Email == user.Email && x.Password == user.Password ).FirstOrDefault();
            
            if(outUser == null)
                return RedirectToAction( Routes.Login, RoutePrefixes.Login );

            Session[StringConstants.UserKey] = outUser;
            FormsAuthentication.SetAuthCookie( outUser.Name, true );

            return RedirectToAction( Routes.Login, RoutePrefixes.Login );
        }

        [Route( Routes.RegisterForm )]
        [HttpGet]
        public ActionResult RegisterForm()
        {
            return View( "register" );
        }

        [Route( Routes.RegisterUser )]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(User user)
        {
            GenericRepository<User> userTable = new GenericRepository<Models.User>( new ApplicationDbContext() );
            var newUser = userTable.Insert( user );

            if( newUser != null )
                ViewBag.Message = string.Format("Registered user with id {0}!",newUser.Id);

            return View( "register" );
        }
    }
}