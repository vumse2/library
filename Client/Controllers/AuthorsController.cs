﻿using Client.Database;
using Models;
using Models.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utils;

namespace Client.Controllers
{
    [RoutePrefix( RoutePrefixes.Author )]
    public class AuthorsController : Controller
    {
        [Route(Routes.AuthorsList)]
        [HttpGet]
        public ActionResult AuthorsList()
        {
            var all = GetAuthors();

            var model = new AuthorsUx { AuthorsList = all };
            return View( Views.AuthorsList, model );
        }

        [Route( Routes.AddAuthor )]
        [HttpPost]
        public ActionResult AddAuthor(AuthorsUx author)
        {
            GenericRepository<Author> userTable = new GenericRepository<Models.Author>( new ApplicationDbContext() );

            var ins = userTable.Insert( author.AuthorToAdd );
            if( ins != null )
                ViewBag.Message = string.Format( "Author with ID = {0} added.", ins.Id );
            else
                ViewBag.Message = "Error inserting author";

            var all = GetAuthors();

            var model = new AuthorsUx { AuthorsList = all };
            return View( Views.AuthorsList, model );
        }

        public IList<Author> GetAuthors()
        {
            GenericRepository<Author> userTable = new GenericRepository<Models.Author>( new ApplicationDbContext() );
            return userTable.GetAll();
        }
    }
}