﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Client
{
    public class ControllerBase : Controller
    {
        protected string GetApiCallUrl( string url )
        {
            return @"http://localhost:49461/" + url;
        }

        protected async Task<T> RequestToApi<T, P>( string url, P param )
            where T : class
        {
            T result = null;
            HttpClient httpClient = new HttpClient();
            var responce = await httpClient.PostAsJsonAsync( GetApiCallUrl( url ), param );
            if( responce.StatusCode == HttpStatusCode.OK )
            {
                result = await responce.Content.ReadAsAsync<T>();

                if( result == null )
                    Console.WriteLine( "Error in api call" );
            }
            else
            {
                Console.WriteLine( "Status code => " + responce.StatusCode );
            }

            return result;
        }
    }
}