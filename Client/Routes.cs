﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Client
{
    public class Routes
    {
        public const string LoginPage = "user_logins";
        public const string Login = "login";
        public const string RegisterForm = "registration_form";
        public const string RegisterUser = "register_user";
        public const string AuthorsList = "authors_list";
        public const string AddAuthor  = "add_author";
        public const string EditAuthor = "edit_author";
    }
}