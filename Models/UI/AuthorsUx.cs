﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.UI
{
    public class AuthorsUx
    {
        public Author AuthorToAdd { get; set; }
        public IList<Author> AuthorsList { get; set; }
    }
}
