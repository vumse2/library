﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        
        public IList<Publisher> Publishers { get; set; }
        public DateTime DatePublished { get; set; }

        public virtual ICollection<Author> Authors { get; set; }
    }
}
