﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string NickName { get; set; }

        public string Name { get; set; }
        public string LastName { get; set; }

        public DateTime DateOfBirth{ get; set; }

        public string Email { get; set; }
        public string Password { get; set; }

        public bool IsAdmin { get; set; }
    }
}
