﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Author
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
